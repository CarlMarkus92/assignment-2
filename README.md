# Assignment 2

## Find all customers
customRepository.findAll();

## Find customer by name
customRepository.findByName("name");

## Find customer by id
customRepository.findById(customer_id);

## Order by limit and offset
customRepository.limitAndOffset(int limit, int offset);

## Find the most visited country
customRepository.findCountry();

## Make a new customer
You first need to make a new Customer object in AppRunner and send inn this object to the method.

Customer customer = new Customer(customer_id, first_name, last_name, country, postal_code, phone, email);
customRepository.insert(customer);

## Update a customers name
customRepository.updateCustomer(customer_id, first_name);

## Find customer who spends the most
customRepository.findHighestSpender();

## Find customers most popular genre
customRepository.findPopularGenreByName(customer_id);

